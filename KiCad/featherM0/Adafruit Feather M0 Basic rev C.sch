EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 11988 8468
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3200 2400 3200 2500
Text Label 3200 2400 0    10   ~ 0
GND
Wire Wire Line
	1400 2400 1400 2500
Text Label 1400 2400 0    10   ~ 0
GND
Wire Wire Line
	2900 2400 2900 2500
Text Label 2900 2400 0    10   ~ 0
GND
Wire Wire Line
	7400 3400 7500 3400
Wire Wire Line
	7500 3400 7500 3600
Text Label 7400 3400 0    10   ~ 0
GND
Wire Wire Line
	4200 2300 4200 2500
Text Label 4200 2300 0    10   ~ 0
GND
Wire Wire Line
	6000 1800 6000 1700
Wire Wire Line
	6000 1600 6000 1700
Connection ~ 6000 1700
Text Label 6000 1800 0    10   ~ 0
GND
Wire Wire Line
	2000 2000 2000 2500
Text Label 2000 2000 0    10   ~ 0
GND
Wire Wire Line
	10600 5500 10600 5600
Text Label 10600 5500 0    10   ~ 0
GND
Wire Wire Line
	9300 4700 9700 4700
Wire Wire Line
	9700 4700 9700 4800
Text Label 9300 4700 0    10   ~ 0
GND
Wire Wire Line
	3800 2500 3800 2200
Text Label 3800 2500 0    10   ~ 0
GND
Wire Wire Line
	9600 2200 9600 2400
Text Label 9600 2200 0    10   ~ 0
GND
Wire Wire Line
	9400 1900 9400 2700
Wire Wire Line
	9400 2700 9400 2800
Wire Wire Line
	8800 2000 8900 2000
Wire Wire Line
	8900 2000 8900 2700
Wire Wire Line
	8900 2700 9400 2700
Connection ~ 9400 2700
Text Label 9400 1900 0    10   ~ 0
GND
Wire Wire Line
	8700 5900 9300 5900
Text Label 8700 5900 0    10   ~ 0
GND
Wire Wire Line
	2000 6600 2000 6800
Connection ~ 2000 6800
Text Label 2000 6600 0    10   ~ 0
GND
Wire Wire Line
	4600 2500 4600 2300
Text Label 4600 2500 0    10   ~ 0
GND
Wire Wire Line
	9300 5500 8700 5500
Text Label 8700 5500 0    70   ~ 0
MOSI
Wire Wire Line
	2000 5600 1600 5600
Text Label 1600 5600 0    70   ~ 0
MOSI
Wire Wire Line
	8700 5600 9300 5600
Text Label 8700 5600 0    70   ~ 0
MISO
Wire Wire Line
	5000 5100 5700 5100
Text Label 5300 5100 0    70   ~ 0
MISO
Wire Wire Line
	9300 5400 8700 5400
Text Label 8700 5400 0    70   ~ 0
SCK
Wire Wire Line
	1600 5700 2000 5700
Text Label 1600 5700 0    70   ~ 0
SCK
Wire Wire Line
	8700 5300 9300 5300
Text Label 8700 5300 0    70   ~ 0
A5
Wire Wire Line
	2000 5200 1600 5200
Text Label 1600 5200 0    70   ~ 0
A5
Wire Wire Line
	9300 5200 8700 5200
Text Label 8700 5200 0    70   ~ 0
A4
Wire Wire Line
	5700 4300 5000 4300
Text Label 5300 4300 0    70   ~ 0
A4
Wire Wire Line
	8700 5100 9300 5100
Text Label 8700 5100 0    70   ~ 0
A3
Wire Wire Line
	5000 4200 5700 4200
Text Label 5300 4200 0    70   ~ 0
A3
Wire Wire Line
	9300 5000 8700 5000
Text Label 8700 5000 0    70   ~ 0
A2
Wire Wire Line
	1600 5500 2000 5500
Text Label 1600 5500 0    70   ~ 0
A2
Wire Wire Line
	8700 4900 9300 4900
Text Label 8700 4900 0    70   ~ 0
A1
Wire Wire Line
	2000 5400 1600 5400
Text Label 1600 5400 0    70   ~ 0
A1
Wire Wire Line
	7900 4900 7100 4900
Text Label 7100 4900 0    70   ~ 0
D11
Wire Wire Line
	5000 5600 5700 5600
Text Label 5300 5600 0    70   ~ 0
D11
Wire Wire Line
	7100 4800 7900 4800
Text Label 7100 4800 0    70   ~ 0
D12
Wire Wire Line
	5000 5900 5700 5900
Text Label 5300 5900 0    70   ~ 0
D12
Wire Wire Line
	4200 2000 4200 1600
Text Label 4200 1900 1    70   ~ 0
AREF
Wire Wire Line
	9300 4600 9700 4600
Text Label 9500 4600 0    70   ~ 0
AREF
Wire Wire Line
	5000 4100 5700 4100
Text Label 5300 4100 0    70   ~ 0
AREF
Wire Wire Line
	2800 1800 2900 1800
Wire Wire Line
	2900 1800 3200 1800
Wire Wire Line
	3200 1800 3200 1400
Wire Wire Line
	3200 1800 3200 2100
Wire Wire Line
	2900 2100 2900 1800
Connection ~ 3200 1800
Connection ~ 2900 1800
Text Label 2800 1800 0    10   ~ 0
+3V3
Wire Wire Line
	8700 4400 8700 4500
Wire Wire Line
	8700 4500 9300 4500
Text Label 8700 4400 0    10   ~ 0
+3V3
Wire Wire Line
	2000 4100 1800 4100
Wire Wire Line
	2000 4200 1800 4200
Wire Wire Line
	1800 4200 1800 4100
Wire Wire Line
	2000 4400 1800 4400
Wire Wire Line
	1800 4400 1800 4200
Connection ~ 1800 4100
Connection ~ 1800 4200
Text Label 2000 4100 0    10   ~ 0
+3V3
Wire Wire Line
	4600 2000 4600 1700
Text Label 4600 2000 0    10   ~ 0
+3V3
Wire Wire Line
	2000 1800 1700 1800
Wire Wire Line
	1700 1800 1400 1800
Wire Wire Line
	1400 1800 1400 2100
Wire Wire Line
	1400 1600 1400 1800
Wire Wire Line
	1200 1400 1200 1800
Wire Wire Line
	1200 1800 1400 1800
Connection ~ 1400 1800
Connection ~ 1700 1800
Text Label 2000 1800 0    10   ~ 0
VBUS
Wire Wire Line
	7400 3000 7500 3000
Wire Wire Line
	7500 3000 7500 2900
Text Label 7400 3000 0    10   ~ 0
VBUS
Wire Wire Line
	7900 4600 6800 4600
Text Label 7900 4600 0    10   ~ 0
VBUS
Wire Wire Line
	6400 1500 6400 1800
Wire Wire Line
	6400 1800 6700 1800
Wire Wire Line
	6700 1800 7800 1800
Wire Wire Line
	6800 2000 6700 2000
Wire Wire Line
	6700 2000 6700 1800
Connection ~ 6700 1800
Text Label 6400 1500 0    10   ~ 0
VBUS
Wire Wire Line
	6800 4400 7900 4400
Text Label 6800 4400 0    10   ~ 0
VBAT
Wire Wire Line
	3800 1700 3800 2100
Text Label 3800 1700 0    10   ~ 0
VBAT
Wire Wire Line
	9600 1800 8800 1800
Wire Wire Line
	9600 1900 9600 1800
Wire Wire Line
	9600 1800 9900 1800
Wire Wire Line
	9900 1400 9900 1800
Connection ~ 9600 1800
Text Label 9600 1800 0    10   ~ 0
VBAT
Wire Wire Line
	10600 5100 10600 5200
Wire Wire Line
	7900 4700 7100 4700
Text Label 7100 4700 0    70   ~ 0
D13
Wire Wire Line
	10600 4700 10600 4500
Text Label 10600 4500 3    70   ~ 0
D13
Wire Wire Line
	5000 5700 5700 5700
Text Label 5300 5700 0    70   ~ 0
D13
Wire Wire Line
	9300 4800 8700 4800
Text Label 8700 4800 0    70   ~ 0
A0
Wire Wire Line
	5000 4000 5700 4000
Text Label 5300 4000 0    70   ~ 0
A0
Wire Wire Line
	7100 2000 7200 2000
Wire Wire Line
	7600 2000 7800 2000
Wire Wire Line
	8800 1900 9000 1900
Wire Wire Line
	2000 4500 1800 4500
Wire Wire Line
	1800 4500 1800 4600
Wire Wire Line
	5000 6400 5700 6400
Text Label 5300 6400 0    70   ~ 0
SCL
Wire Wire Line
	7900 5400 7100 5400
Text Label 7100 5400 0    70   ~ 0
SCL
Wire Wire Line
	5000 6300 5700 6300
Text Label 5300 6300 0    70   ~ 0
SDA
Wire Wire Line
	7100 5500 7900 5500
Text Label 7100 5500 0    70   ~ 0
SDA
Wire Wire Line
	5000 4500 5700 4500
Text Label 5300 4500 0    70   ~ 0
D9
Wire Wire Line
	7900 5100 7100 5100
Text Label 7100 5100 0    70   ~ 0
D9
Wire Wire Line
	10600 3200 10200 3200
Connection ~ 10600 3200
Text Label 10300 3200 0    70   ~ 0
D9
Wire Wire Line
	5000 4400 5700 4400
Text Label 5300 4400 0    70   ~ 0
D8
Wire Wire Line
	5000 6100 5700 6100
Text Label 5300 6100 0    70   ~ 0
D7
Wire Wire Line
	5000 6000 5700 6000
Text Label 5300 6000 0    70   ~ 0
D6
Wire Wire Line
	7100 5200 7900 5200
Text Label 7100 5200 0    70   ~ 0
D6
Wire Wire Line
	5700 5500 5000 5500
Text Label 5300 5500 0    70   ~ 0
D5
Wire Wire Line
	7900 5300 7100 5300
Text Label 7100 5300 0    70   ~ 0
D5
Wire Wire Line
	5700 4700 5000 4700
Text Label 5300 4700 0    70   ~ 0
D4
Wire Wire Line
	5000 4900 5700 4900
Text Label 5300 4900 0    70   ~ 0
D1
Wire Wire Line
	8700 5800 9300 5800
Text Label 8700 5800 0    70   ~ 0
D1
Wire Wire Line
	5700 5000 5000 5000
Text Label 5300 5000 0    70   ~ 0
D0
Wire Wire Line
	9300 5700 8700 5700
Text Label 8700 5700 0    70   ~ 0
D0
Wire Wire Line
	7100 5000 7900 5000
Text Label 7100 5000 0    70   ~ 0
D10
Wire Wire Line
	5700 5800 5000 5800
Text Label 5300 5800 0    70   ~ 0
D10
Wire Wire Line
	7400 3100 8000 3100
Text Label 7600 3100 0    70   ~ 0
D+
Wire Wire Line
	5700 6600 5000 6600
Text Label 5300 6600 0    70   ~ 0
D+
Wire Wire Line
	8000 3200 7400 3200
Text Label 7600 3200 0    70   ~ 0
D-
Wire Wire Line
	5000 6500 5700 6500
Text Label 5300 6500 0    70   ~ 0
D-
Wire Wire Line
	2000 3900 1300 3900
Text Label 1400 3900 0    70   ~ 0
~RESET
Wire Wire Line
	5600 1700 5600 1600
Wire Wire Line
	5400 1700 5600 1700
Connection ~ 5600 1700
Text Label 5400 1700 0    70   ~ 0
~RESET
Wire Wire Line
	9300 4400 8800 4400
Text Label 8800 4400 0    70   ~ 0
~RESET
Wire Wire Line
	5000 6900 5700 6900
Text Label 5300 6900 0    70   ~ 0
SWCLK
Wire Wire Line
	5700 7000 5000 7000
Text Label 5300 7000 0    70   ~ 0
SWDIO
Wire Wire Line
	5000 3800 5200 3800
Wire Wire Line
	5200 3800 5200 3700
Wire Wire Line
	5200 3500 5200 3700
Connection ~ 5200 3700
Wire Wire Line
	5000 3900 5600 3900
Wire Wire Line
	5600 3900 5600 3700
Wire Wire Line
	5600 3700 5600 3500
Connection ~ 5600 3700
Wire Wire Line
	1700 2200 1900 2200
Wire Wire Line
	1900 2200 1900 1900
Wire Wire Line
	1900 1900 2000 1900
Text Label 1800 2200 0    70   ~ 0
EN
Wire Wire Line
	7100 4500 7900 4500
Text Label 7100 4500 0    70   ~ 0
EN
Wire Wire Line
	5000 4800 5700 4800
Text Label 5400 4800 0    70   ~ 0
D3
Wire Wire Line
	5000 5400 5700 5400
Text Label 5300 5400 0    70   ~ 0
D2
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:FRAME_A4 #FRAME1
U 2 1 CC96BE8F
P 6200 7500
F 0 "#FRAME1" H 6200 7500 50  0001 C CNN
F 1 "FRAME_A4" H 6200 7500 50  0001 C CNN
F 2 "" H 6200 7500 50  0001 C CNN
F 3 "" H 6200 7500 50  0001 C CNN
	2    6200 7500
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0805-NOOUTLINE C6
U 1 1 6333BD7E
P 1400 2300
F 0 "C6" V 1310 2349 50  0000 C CNN
F 1 "10µF" V 1490 2349 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0805-NO" H 1400 2300 50  0001 C CNN
F 3 "" H 1400 2300 50  0001 C CNN
	1    1400 2300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0603_NO C7
U 1 1 A5E7C4B6
P 3200 2300
F 0 "C7" V 3110 2349 50  0000 C CNN
F 1 "1uF" V 3290 2349 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 3200 2300 50  0001 C CNN
F 3 "" H 3200 2300 50  0001 C CNN
	1    3200 2300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$027
U 1 1 88B45EA1
P 1400 2600
F 0 "#U$027" H 1400 2600 50  0001 C CNN
F 1 "GND" H 1300 2500 59  0000 L BNN
F 2 "" H 1400 2600 50  0001 C CNN
F 3 "" H 1400 2600 50  0001 C CNN
	1    1400 2600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$028
U 1 1 EC169927
P 3200 2600
F 0 "#U$028" H 3200 2600 50  0001 C CNN
F 1 "GND" H 3100 2500 59  0000 L BNN
F 2 "" H 3200 2600 50  0001 C CNN
F 3 "" H 3200 2600 50  0001 C CNN
	1    3200 2600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0805-NOOUTLINE C8
U 1 1 4085EC5D
P 2900 2300
F 0 "C8" V 2810 2349 50  0000 C CNN
F 1 "10µF" V 2990 2349 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0805-NO" H 2900 2300 50  0001 C CNN
F 3 "" H 2900 2300 50  0001 C CNN
	1    2900 2300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$029
U 1 1 9452B865
P 2900 2600
F 0 "#U$029" H 2900 2600 50  0001 C CNN
F 1 "GND" H 2800 2500 59  0000 L BNN
F 2 "" H 2900 2600 50  0001 C CNN
F 3 "" H 2900 2600 50  0001 C CNN
	1    2900 2600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$030
U 1 1 A87AFBDE
P 2000 2600
F 0 "#U$030" H 2000 2600 50  0001 C CNN
F 1 "GND" H 1900 2500 59  0000 L BNN
F 2 "" H 2000 2600 50  0001 C CNN
F 3 "" H 2000 2600 50  0001 C CNN
	1    2000 2600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND01
U 1 1 8DA2BB24
P 7500 3700
F 0 "#GND01" H 7500 3700 50  0001 C CNN
F 1 "GND" H 7400 3600 59  0000 L BNN
F 2 "" H 7500 3700 50  0001 C CNN
F 3 "" H 7500 3700 50  0001 C CNN
	1    7500 3700
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND012
U 1 1 8C4C494A
P 4200 2600
F 0 "#GND012" H 4200 2600 50  0001 C CNN
F 1 "GND" H 4100 2500 59  0000 L BNN
F 2 "" H 4200 2600 50  0001 C CNN
F 3 "" H 4200 2600 50  0001 C CNN
	1    4200 2600
	-1   0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:LED0805_NOOUTLINE L1
U 1 1 B2CE564C
P 10600 5400
F 0 "L1" H 10550 5575 42  0000 C CNN
F 1 "RED" H 10550 5290 42  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:CHIPLED_0805_NOOUTLINE" H 10600 5400 50  0001 C CNN
F 3 "" H 10600 5400 50  0001 C CNN
	1    10600 5400
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND07
U 1 1 C2693486
P 6000 1900
F 0 "#GND07" H 6000 1900 50  0001 C CNN
F 1 "GND" H 5900 1800 59  0000 L BNN
F 2 "" H 6000 1900 50  0001 C CNN
F 3 "" H 6000 1900 50  0001 C CNN
	1    6000 1900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0603_NO C14
U 1 1 711FF4DB
P 4200 2200
F 0 "C14" V 4110 2249 50  0000 C CNN
F 1 "1uF" V 4290 2249 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 4200 2200 50  0001 C CNN
F 3 "" H 4200 2200 50  0001 C CNN
	1    4200 2200
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:RESISTOR_0603_NOOUT R7
U 1 1 2172FC05
P 10600 4900
F 0 "R7" H 10600 5000 50  0000 C CNN
F 1 "2.2K" H 10600 4900 40  0000 C CNB
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 10600 4900 50  0001 C CNN
F 3 "" H 10600 4900 50  0001 C CNN
	1    10600 4900
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND010
U 1 1 0087CD10
P 10600 5700
F 0 "#GND010" H 10600 5700 50  0001 C CNN
F 1 "GND" H 10500 5600 59  0000 L BNN
F 2 "" H 10600 5700 50  0001 C CNN
F 3 "" H 10600 5700 50  0001 C CNN
	1    10600 5700
	-1   0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:FIDUCIAL"" U$34
U 1 1 1AF98053
P 9200 6900
F 0 "U$34" H 9200 6900 50  0001 C CNN
F 1 "FIDUCIAL\"\"" H 9200 6900 50  0001 C CNN
F 2 "Adafruit Feather M0 Basic rev C:FIDUCIAL_1MM" H 9200 6900 50  0001 C CNN
F 3 "" H 9200 6900 50  0001 C CNN
	1    9200 6900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:FIDUCIAL"" U$35
U 1 1 B9D1E2C2
P 9000 6900
F 0 "U$35" H 9000 6900 50  0001 C CNN
F 1 "FIDUCIAL\"\"" H 9000 6900 50  0001 C CNN
F 2 "Adafruit Feather M0 Basic rev C:FIDUCIAL_1MM" H 9000 6900 50  0001 C CNN
F 3 "" H 9000 6900 50  0001 C CNN
	1    9000 6900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:SPST_TACT-KMR2 SW1
U 1 1 F85A3293
P 5800 1600
F 0 "SW1" V 5550 1500 59  0000 L BNN
F 1 "SPST_TACT-KMR2" V 5825 1550 59  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:KMR2" H 5800 1600 50  0001 C CNN
F 3 "" H 5800 1600 50  0001 C CNN
	1    5800 1600
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:MOUNTINGHOLE2.5 U$31
U 1 1 BD08BF3C
P 9000 7100
F 0 "U$31" H 9000 7100 50  0001 C CNN
F 1 "MOUNTINGHOLE2.5" H 9000 7100 50  0001 C CNN
F 2 "Adafruit Feather M0 Basic rev C:MOUNTINGHOLE_2.5_PLATED" H 9000 7100 50  0001 C CNN
F 3 "" H 9000 7100 50  0001 C CNN
	1    9000 7100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:MOUNTINGHOLE2.5 U$32
U 1 1 F4AAF165
P 9200 7100
F 0 "U$32" H 9200 7100 50  0001 C CNN
F 1 "MOUNTINGHOLE2.5" H 9200 7100 50  0001 C CNN
F 2 "Adafruit Feather M0 Basic rev C:MOUNTINGHOLE_2.5_PLATED" H 9200 7100 50  0001 C CNN
F 3 "" H 9200 7100 50  0001 C CNN
	1    9200 7100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:+3V3 #+3V04
U 1 1 3511C8A8
P 3200 1300
F 0 "#+3V04" H 3200 1300 50  0001 C CNN
F 1 "+3V3" V 3100 1100 59  0000 L BNN
F 2 "" H 3200 1300 50  0001 C CNN
F 3 "" H 3200 1300 50  0001 C CNN
	1    3200 1300
	-1   0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBUS #U$01
U 1 1 89E33A62
P 7500 2800
F 0 "#U$01" H 7500 2800 50  0001 C CNN
F 1 "VBUS" H 7440 2840 42  0000 L BNN
F 2 "" H 7500 2800 50  0001 C CNN
F 3 "" H 7500 2800 50  0001 C CNN
	1    7500 2800
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBUS #U$03
U 1 1 6E1B97F5
P 1200 1300
F 0 "#U$03" H 1200 1300 50  0001 C CNN
F 1 "VBUS" H 1140 1340 42  0000 L BNN
F 2 "" H 1200 1300 50  0001 C CNN
F 3 "" H 1200 1300 50  0001 C CNN
	1    1200 1300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBAT #U$021
U 1 1 CA2824D4
P 1400 1300
F 0 "#U$021" H 1400 1300 50  0001 C CNN
F 1 "VBAT" H 1340 1340 42  0000 L BNN
F 2 "" H 1400 1300 50  0001 C CNN
F 3 "" H 1400 1300 50  0001 C CNN
	1    1400 1300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:USB_MICRO_20329_V2 X3
U 1 1 82D7A832
P 7000 3200
F 0 "X3" H 6600 3540 42  0000 L BNN
F 1 "microUSB" H 6600 2800 42  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:4UCONN_20329_V2" H 7000 3200 50  0001 C CNN
F 3 "" H 7000 3200 50  0001 C CNN
	1    7000 3200
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VREG_SOT23-5 U2
U 1 1 49C2BC65
P 2400 1900
F 0 "U2" H 2100 2140 42  0000 L BNN
F 1 "SPX3819-3.3" H 2100 1600 42  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:SOT23-5" H 2400 1900 50  0001 C CNN
F 3 "" H 2400 1900 50  0001 C CNN
	1    2400 1900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:DIODE-SCHOTTKYSOD-123 D1
U 1 1 2438F767
P 1400 1500
F 0 "D1" H 1400 1600 42  0000 C CNN
F 1 "MBR120" H 1400 1402 42  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:SOD-123" H 1400 1500 50  0001 C CNN
F 3 "" H 1400 1500 50  0001 C CNN
	1    1400 1500
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBUS #U$019
U 1 1 1DEA58DA
P 6700 4600
F 0 "#U$019" H 6700 4600 50  0001 C CNN
F 1 "VBUS" H 6640 4640 42  0000 L BNN
F 2 "" H 6700 4600 50  0001 C CNN
F 3 "" H 6700 4600 50  0001 C CNN
	1    6700 4600
	0    -1   -1   0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBAT #U$020
U 1 1 A9A82E2E
P 6700 4400
F 0 "#U$020" H 6700 4400 50  0001 C CNN
F 1 "VBAT" H 6640 4440 42  0000 L BNN
F 2 "" H 6700 4400 50  0001 C CNN
F 3 "" H 6700 4400 50  0001 C CNN
	1    6700 4400
	0    -1   -1   0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:HEADER-1X16ROUND JP1
U 1 1 0B60073D
P 9200 5200
F 0 "JP1" H 8950 6025 59  0000 L BNN
F 1 "HEADER-1X16ROUND" H 8950 4200 59  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:1X16_ROUND" H 9200 5200 50  0001 C CNN
F 3 "" H 9200 5200 50  0001 C CNN
	1    9200 5200
	-1   0    0    1   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:+3V3 #+3V01
U 1 1 7D977A11
P 8700 4300
F 0 "#+3V01" H 8700 4300 50  0001 C CNN
F 1 "+3V3" V 8600 4100 59  0000 L BNN
F 2 "" H 8700 4300 50  0001 C CNN
F 3 "" H 8700 4300 50  0001 C CNN
	1    8700 4300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND04
U 1 1 12C122FD
P 9700 4900
F 0 "#GND04" H 9700 4900 50  0001 C CNN
F 1 "GND" H 9600 4800 59  0000 L BNN
F 2 "" H 9700 4900 50  0001 C CNN
F 3 "" H 9700 4900 50  0001 C CNN
	1    9700 4900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CON_JST_PH_2PIN X1
U 1 1 8C6B6C8F
P 3700 2100
F 0 "X1" H 3450 2325 59  0000 L BNN
F 1 "JSTPH" H 3450 1900 59  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:JSTPH2" H 3700 2100 50  0001 C CNN
F 3 "" H 3700 2100 50  0001 C CNN
	1    3700 2100
	-1   0    0    1   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBAT #U$016
U 1 1 44B01CF5
P 3800 1600
F 0 "#U$016" H 3800 1600 50  0001 C CNN
F 1 "VBAT" H 3740 1640 42  0000 L BNN
F 2 "" H 3800 1600 50  0001 C CNN
F 3 "" H 3800 1600 50  0001 C CNN
	1    3800 1600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$022
U 1 1 585CEF8C
P 3800 2600
F 0 "#U$022" H 3800 2600 50  0001 C CNN
F 1 "GND" H 3700 2500 59  0000 L BNN
F 2 "" H 3800 2600 50  0001 C CNN
F 3 "" H 3800 2600 50  0001 C CNN
	1    3800 2600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:FRAME_A4 #FRAME1
U 1 1 CC96BE83
P 900 7600
F 0 "#FRAME1" H 900 7600 50  0001 C CNN
F 1 "FRAME_A4" H 900 7600 50  0001 C CNN
F 2 "" H 900 7600 50  0001 C CNN
F 3 "" H 900 7600 50  0001 C CNN
	1    900  7600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:HEADER-1X12 JP3
U 1 1 8182258E
P 8000 5000
F 0 "JP3" H 7750 5725 59  0000 L BNN
F 1 "HEADER-1X12" H 7750 4300 59  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:1X12_ROUND" H 8000 5000 50  0001 C CNN
F 3 "" H 8000 5000 50  0001 C CNN
	1    8000 5000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:MCP73831_2 U3
U 1 1 CDA6E5AA
P 8300 1900
F 0 "U3" H 7900 2350 42  0000 L BNN
F 1 "MCP73831T-2ACI/OT" H 7900 1400 42  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:SOT23-5" H 8300 1900 50  0001 C CNN
F 3 "" H 8300 1900 50  0001 C CNN
	1    8300 1900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:LED0805_NOOUTLINE CHG1
U 1 1 84E678BB
P 7000 2000
F 0 "CHG1" H 6950 2175 42  0000 C CNN
F 1 "ORANGE" H 6950 1890 42  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:CHIPLED_0805_NOOUTLINE" H 7000 2000 50  0001 C CNN
F 3 "" H 7000 2000 50  0001 C CNN
	1    7000 2000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:RESISTOR_0603_NOOUT R2
U 1 1 FA6D7899
P 7400 2000
F 0 "R2" H 7400 2100 50  0000 C CNN
F 1 "1K" H 7400 2000 40  0000 C CNB
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 7400 2000 50  0001 C CNN
F 3 "" H 7400 2000 50  0001 C CNN
	1    7400 2000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0805-NOOUTLINE C3
U 1 1 6F05E4D1
P 9600 2100
F 0 "C3" V 9510 2149 50  0000 C CNN
F 1 "10µF" V 9690 2149 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0805-NO" H 9600 2100 50  0001 C CNN
F 3 "" H 9600 2100 50  0001 C CNN
	1    9600 2100
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$033
U 1 1 6953EFA5
P 9600 2500
F 0 "#U$033" H 9600 2500 50  0001 C CNN
F 1 "GND" H 9500 2400 59  0000 L BNN
F 2 "" H 9600 2500 50  0001 C CNN
F 3 "" H 9600 2500 50  0001 C CNN
	1    9600 2500
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:RESISTOR_0603_NOOUT R8
U 1 1 F23A12C2
P 9200 1900
F 0 "R8" H 9200 2000 50  0000 C CNN
F 1 "10K\\" H 9200 1900 40  0000 C CNB
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 9200 1900 50  0001 C CNN
F 3 "" H 9200 1900 50  0001 C CNN
	1    9200 1900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$036
U 1 1 BA2602F5
P 9400 2900
F 0 "#U$036" H 9400 2900 50  0001 C CNN
F 1 "GND" H 9300 2800 59  0000 L BNN
F 2 "" H 9400 2900 50  0001 C CNN
F 3 "" H 9400 2900 50  0001 C CNN
	1    9400 2900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBUS #U$038
U 1 1 D00605AD
P 6400 1400
F 0 "#U$038" H 6400 1400 50  0001 C CNN
F 1 "VBUS" H 6340 1440 42  0000 L BNN
F 2 "" H 6400 1400 50  0001 C CNN
F 3 "" H 6400 1400 50  0001 C CNN
	1    6400 1400
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBAT #U$039
U 1 1 1511A889
P 9900 1300
F 0 "#U$039" H 9900 1300 50  0001 C CNN
F 1 "VBAT" H 9840 1340 42  0000 L BNN
F 2 "" H 9900 1300 50  0001 C CNN
F 3 "" H 9900 1300 50  0001 C CNN
	1    9900 1300
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND011
U 1 1 5518330F
P 8700 6000
F 0 "#GND011" H 8700 6000 50  0001 C CNN
F 1 "GND" H 8600 5900 59  0000 L BNN
F 2 "" H 8700 6000 50  0001 C CNN
F 3 "" H 8700 6000 50  0001 C CNN
	1    8700 6000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:RESISTOR_0603_NOOUT R3
U 1 1 94754E9C
P 10600 3000
F 0 "R3" H 10600 3100 50  0000 C CNN
F 1 "100k" H 10600 3000 40  0000 C CNB
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 10600 3000 50  0001 C CNN
F 3 "" H 10600 3000 50  0001 C CNN
	1    10600 3000
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:RESISTOR_0603_NOOUT R6
U 1 1 E5D7C87D
P 10600 3400
F 0 "R6" H 10600 3500 50  0000 C CNN
F 1 "100K" H 10600 3400 40  0000 C CNB
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 10600 3400 50  0001 C CNN
F 3 "" H 10600 3400 50  0001 C CNN
	1    10600 3400
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:VBAT #U$02
U 1 1 87B7DF34
P 10600 2700
F 0 "#U$02" H 10600 2700 50  0001 C CNN
F 1 "VBAT" H 10540 2740 42  0000 L BNN
F 2 "" H 10600 2700 50  0001 C CNN
F 3 "" H 10600 2700 50  0001 C CNN
	1    10600 2700
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #GND013
U 1 1 85B66688
P 10600 3700
F 0 "#GND013" H 10600 3700 50  0001 C CNN
F 1 "GND" H 10500 3600 59  0000 L BNN
F 2 "" H 10600 3700 50  0001 C CNN
F 3 "" H 10600 3700 50  0001 C CNN
	1    10600 3700
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:ATSAMD21J_QFN U$4
U 1 1 6C1C0051
P 3000 5200
F 0 "U$4" H 2200 3300 59  0000 L BNN
F 1 "ATSAMD21G18_QFN" H 2400 6800 59  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:TQFN48_7MM" H 3000 5200 50  0001 C CNN
F 3 "" H 3000 5200 50  0001 C CNN
	1    3000 5200
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$05
U 1 1 BCE09641
P 2000 6900
F 0 "#U$05" H 2000 6900 50  0001 C CNN
F 1 "GND" H 1900 6800 59  0000 L BNN
F 2 "" H 2000 6900 50  0001 C CNN
F 3 "" H 2000 6900 50  0001 C CNN
	1    2000 6900
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:+3V3 #+3V03
U 1 1 EB5F8C4A
P 1800 4000
F 0 "#+3V03" H 1800 4000 50  0001 C CNN
F 1 "+3V3" V 1700 3800 59  0000 L BNN
F 2 "" H 1800 4000 50  0001 C CNN
F 3 "" H 1800 4000 50  0001 C CNN
	1    1800 4000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0603_NO C1
U 1 1 C8E3AF05
P 1800 4800
F 0 "C1" V 1710 4849 50  0000 C CNN
F 1 "1uF" V 1890 4849 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 1800 4800 50  0001 C CNN
F 3 "" H 1800 4800 50  0001 C CNN
	1    1800 4800
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$06
U 1 1 CE08125E
P 1800 5000
F 0 "#U$06" H 1800 5000 50  0001 C CNN
F 1 "GND" H 1700 4900 59  0000 L BNN
F 2 "" H 1800 5000 50  0001 C CNN
F 3 "" H 1800 5000 50  0001 C CNN
	1    1800 5000
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0603_NO C2
U 1 1 1A664D53
P 5200 3300
F 0 "C2" V 5110 3349 50  0000 C CNN
F 1 "22pF" V 5290 3349 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 5200 3300 50  0001 C CNN
F 3 "" H 5200 3300 50  0001 C CNN
	1    5200 3300
	-1   0    0    1   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0603_NO C4
U 1 1 D14D4BF0
P 5600 3300
F 0 "C4" V 5510 3349 50  0000 C CNN
F 1 "22pF" V 5690 3349 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 5600 3300 50  0001 C CNN
F 3 "" H 5600 3300 50  0001 C CNN
	1    5600 3300
	-1   0    0    1   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$08
U 1 1 59F585D1
P 5200 3100
F 0 "#U$08" H 5200 3100 50  0001 C CNN
F 1 "GND" H 5100 3000 59  0000 L BNN
F 2 "" H 5200 3100 50  0001 C CNN
F 3 "" H 5200 3100 50  0001 C CNN
	1    5200 3100
	-1   0    0    1   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$010
U 1 1 2B263A79
P 5600 3100
F 0 "#U$010" H 5600 3100 50  0001 C CNN
F 1 "GND" H 5500 3000 59  0000 L BNN
F 2 "" H 5600 3100 50  0001 C CNN
F 3 "" H 5600 3100 50  0001 C CNN
	1    5600 3100
	-1   0    0    1   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:XTAL-3.2X1.5 X2
U 1 1 211F99F7
P 5400 3700
F 0 "X2" H 5300 3850 50  0000 L BNN
F 1 "32.768" H 5250 3500 50  0000 L BNN
F 2 "Adafruit Feather M0 Basic rev C:XTAL3215" H 5400 3700 50  0001 C CNN
F 3 "" H 5400 3700 50  0001 C CNN
	1    5400 3700
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:CAP_CERAMIC0805-NOOUTLINE C5
U 1 1 607D4452
P 4600 2200
F 0 "C5" V 4510 2249 50  0000 C CNN
F 1 "10uF" V 4690 2249 50  0000 C CNN
F 2 "Adafruit Feather M0 Basic rev C:0805-NO" H 4600 2200 50  0001 C CNN
F 3 "" H 4600 2200 50  0001 C CNN
	1    4600 2200
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:+3V3 #+3V02
U 1 1 816AD438
P 4600 1600
F 0 "#+3V02" H 4600 1600 50  0001 C CNN
F 1 "+3V3" V 4500 1400 59  0000 L BNN
F 2 "" H 4600 1600 50  0001 C CNN
F 3 "" H 4600 1600 50  0001 C CNN
	1    4600 1600
	-1   0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:GND #U$011
U 1 1 ABBF7E6D
P 4600 2600
F 0 "#U$011" H 4600 2600 50  0001 C CNN
F 1 "GND" H 4500 2500 59  0000 L BNN
F 2 "" H 4600 2600 50  0001 C CNN
F 3 "" H 4600 2600 50  0001 C CNN
	1    4600 2600
	1    0    0    -1  
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:TESTPOINTROUND1.5MM TP1
U 1 1 FE5B29FD
P 5700 6900
F 0 "TP1" V 5700 7120 42  0000 L CNN
F 1 "TESTPOINTROUND1.5MM" V 5765 7120 42  0000 L CNN
F 2 "Adafruit Feather M0 Basic rev C:TESTPOINT_ROUND_1.5MM" H 5700 6900 50  0001 C CNN
F 3 "" H 5700 6900 50  0001 C CNN
	1    5700 6900
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:TESTPOINTROUND1.5MM TP2
U 1 1 5A8AD4FA
P 5700 7000
F 0 "TP2" V 5700 7220 42  0000 L CNN
F 1 "TESTPOINTROUND1.5MM" V 5765 7220 42  0000 L CNN
F 2 "Adafruit Feather M0 Basic rev C:TESTPOINT_ROUND_1.5MM" H 5700 7000 50  0001 C CNN
F 3 "" H 5700 7000 50  0001 C CNN
	1    5700 7000
	0    1    1    0   
$EndComp
$Comp
L Adafruit_Feather_M0_Basic_rev_C-eagle-import:RESISTOR_0603_NOOUT R1
U 1 1 6019A4D2
P 1700 2000
F 0 "R1" H 1700 2100 50  0000 C CNN
F 1 "100k" H 1700 2000 40  0000 C CNB
F 2 "Adafruit Feather M0 Basic rev C:0603-NO" H 1700 2000 50  0001 C CNN
F 3 "" H 1700 2000 50  0001 C CNN
	1    1700 2000
	0    1    1    0   
$EndComp
Text Notes 2600 900  0    59   ~ 0
POWER AND FILTERING
Wire Notes Line
	5300 2800 5300 700 
Text Notes 5600 900  0    59   ~ 0
RESET
Wire Notes Line
	10200 6100 10200 4100
Text Notes 8900 1300 0    59   ~ 0
10K  = 100mA
Text Notes 8900 1400 0    59   ~ 0
5.0K  = 200mA
Text Notes 8900 1500 0    59   ~ 0
2.0K  = 500mA
Text Notes 8900 1600 0    59   ~ 0
1.0K  = 1000mA
Text Notes 5300 5200 0    59   ~ 0
N/C
Text Notes 5300 6700 0    59   ~ 0
TXLED
Text Notes 5300 6800 0    59   ~ 0
USBHOSTEN
Text Notes 1600 5300 0    59   ~ 0
RXLED
Text Notes 1600 5800 0    59   ~ 0
TXD
Text Notes 1600 5900 0    59   ~ 0
RXD
Text Notes 7500 1000 0    59   ~ 0
USB AND BATTERY CHARGING
Text Notes 7100 5900 0    59   ~ 0
BREAKOUTS
Text Notes 10300 4300 0    59   ~ 0
RED LED
Wire Notes Line
	1100 2800 6200 2800
Wire Notes Line
	6200 700  6200 6100
Wire Notes Line
	6200 4100 11000 4100
$EndSCHEMATC
