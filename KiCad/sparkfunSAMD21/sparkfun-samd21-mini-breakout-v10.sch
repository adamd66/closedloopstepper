EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 11844 8868
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2800 7100 2700 7100
Wire Wire Line
	2700 7100 2700 7300
Wire Wire Line
	2800 7000 2700 7000
Wire Wire Line
	2700 7000 2700 7100
Wire Wire Line
	2800 6900 2700 6900
Wire Wire Line
	2700 6900 2700 7000
Wire Wire Line
	2800 6700 2700 6700
Wire Wire Line
	2700 6700 2700 6900
Connection ~ 2700 7100
Connection ~ 2700 7000
Connection ~ 2700 6900
Text Label 2800 7100 0    10   ~ 0
GND
Wire Wire Line
	2200 4900 2200 4800
Text Label 2200 4900 0    10   ~ 0
GND
Wire Wire Line
	2700 4800 2700 4900
Text Label 2700 4800 0    10   ~ 0
GND
Wire Wire Line
	1700 5600 1700 5500
Text Label 1700 5600 0    10   ~ 0
GND
Wire Wire Line
	2100 5500 2000 5500
Wire Wire Line
	2000 5500 2000 5600
Wire Wire Line
	2100 5300 2000 5300
Wire Wire Line
	2000 5300 2000 5500
Connection ~ 2000 5500
Text Label 2100 5500 0    10   ~ 0
GND
Wire Wire Line
	6200 4600 6200 4500
Text Label 6200 4600 0    10   ~ 0
GND
Wire Wire Line
	2200 1800 2300 1800
Wire Wire Line
	2300 1800 2300 2000
Wire Wire Line
	2200 1300 2300 1300
Wire Wire Line
	2300 1300 2300 1800
Connection ~ 2300 1800
Text Label 2200 1800 0    10   ~ 0
GND
Wire Wire Line
	2200 6900 2300 6900
Wire Wire Line
	2300 6900 2300 7000
Text Label 2200 6900 0    10   ~ 0
GND
Wire Wire Line
	5600 1800 5500 1800
Wire Wire Line
	5500 1800 5500 1900
Wire Wire Line
	5500 1900 5500 2000
Wire Wire Line
	5200 1800 5200 1900
Wire Wire Line
	5200 1900 5500 1900
Connection ~ 5500 1900
Text Label 5600 1800 0    10   ~ 0
GND
Wire Wire Line
	6500 2000 6500 1800
Text Label 6500 2000 0    10   ~ 0
GND
Wire Wire Line
	8000 5400 8100 5400
Wire Wire Line
	8100 5400 8100 6300
Text Label 8000 5400 0    10   ~ 0
GND
Wire Wire Line
	9400 5200 9300 5200
Wire Wire Line
	9300 5200 9300 6300
Text Label 9400 5200 0    10   ~ 0
GND
Wire Wire Line
	7400 2800 7400 2700
Text Label 7400 2800 0    10   ~ 0
GND
Wire Wire Line
	8300 3900 8200 3900
Wire Wire Line
	8200 3900 8200 4000
Wire Wire Line
	8200 4000 8200 4300
Wire Wire Line
	8300 4000 8200 4000
Connection ~ 8200 4000
Text Label 8300 3900 0    10   ~ 0
GND
Wire Wire Line
	3200 2800 3100 2800
Wire Wire Line
	3100 2800 3100 2900
Text Label 3200 2800 0    10   ~ 0
GND
Wire Wire Line
	9400 2700 9400 2800
Text Label 9400 2700 0    10   ~ 0
GND
Wire Wire Line
	2700 3600 2700 3700
Text Label 2700 3600 0    10   ~ 0
3.3V
Wire Wire Line
	2800 4300 2200 4300
Wire Wire Line
	2200 4300 2200 4500
Wire Wire Line
	2200 4300 2200 3600
Connection ~ 2200 4300
Text Label 2800 4300 0    10   ~ 0
3.3V
Wire Wire Line
	1700 4400 1700 4300
Text Label 1700 4400 0    10   ~ 0
3.3V
Wire Wire Line
	5500 4300 5900 4300
Wire Wire Line
	5900 4300 5900 4200
Wire Wire Line
	5900 4200 5900 4100
Wire Wire Line
	5900 4100 6200 4100
Wire Wire Line
	6200 4100 6200 4200
Wire Wire Line
	6200 4100 6200 3800
Wire Wire Line
	5500 4200 5900 4200
Connection ~ 6200 4100
Connection ~ 5900 4200
Text Label 5500 4300 0    10   ~ 0
3.3V
Wire Wire Line
	6300 1400 6400 1400
Wire Wire Line
	6400 1400 6500 1400
Wire Wire Line
	6500 1400 6500 1500
Wire Wire Line
	6400 1400 6400 1300
Connection ~ 6400 1400
Text Label 6300 1400 0    10   ~ 0
3.3V
Wire Wire Line
	9400 1300 9400 1400
Text Label 9400 1300 0    10   ~ 0
3.3V
Wire Wire Line
	9400 5400 9200 5400
Wire Wire Line
	9200 5400 9200 5000
Text Label 9400 5400 0    10   ~ 0
3.3V
Wire Wire Line
	7400 1300 7400 1400
Text Label 7400 1300 0    10   ~ 0
3.3V
Wire Wire Line
	8300 3800 8200 3800
Wire Wire Line
	8200 3800 8200 3700
Text Label 8300 3800 0    10   ~ 0
3.3V
Wire Wire Line
	8000 1300 8000 1400
Text Label 8000 1300 0    10   ~ 0
3.3V
Wire Wire Line
	8700 1300 8700 1400
Text Label 8700 1300 0    10   ~ 0
3.3V
Wire Wire Line
	2800 4400 2700 4400
Wire Wire Line
	2700 4400 2700 4500
Wire Wire Line
	2800 5100 1700 5100
Wire Wire Line
	1700 5100 1700 5000
Wire Wire Line
	1700 5200 1700 5100
Connection ~ 1700 5100
Wire Wire Line
	2800 5300 2500 5300
Wire Wire Line
	2500 5300 2400 5300
Connection ~ 2500 5300
Wire Wire Line
	2400 5500 2500 5500
Wire Wire Line
	2500 5500 2800 5500
Connection ~ 2500 5500
Wire Wire Line
	2800 4200 2700 4200
Wire Wire Line
	2700 4200 2700 4100
Wire Wire Line
	2700 4200 2600 4200
Connection ~ 2700 4200
Text Label 2600 4200 2    50   ~ 0
~RESET
Wire Wire Line
	1800 6900 1700 6900
Text Label 1700 6900 2    50   ~ 0
~RESET
Wire Wire Line
	8000 5300 8200 5300
Text Label 8200 5300 0    50   ~ 0
~RESET
Wire Wire Line
	9600 4200 9700 4200
Text Label 9700 4200 0    50   ~ 0
~RESET
Wire Wire Line
	5500 6200 5600 6200
Text Label 5600 6200 0    50   ~ 0
USB_D+
Wire Wire Line
	2200 1600 2500 1600
Text Label 2500 1600 0    50   ~ 0
USB_D+
Wire Wire Line
	5500 6100 5600 6100
Text Label 5600 6100 0    50   ~ 0
USB_D-
Wire Wire Line
	2200 1500 2500 1500
Text Label 2500 1500 0    50   ~ 0
USB_D-
Wire Wire Line
	8000 5100 8200 5100
Text Label 8200 5100 0    50   ~ 0
D1/TXO
Wire Wire Line
	5500 4700 5600 4700
Text Label 5600 4700 0    50   ~ 0
D1/TXO
Wire Wire Line
	8000 5200 8200 5200
Text Label 8200 5200 0    50   ~ 0
D0/RXI
Wire Wire Line
	5500 4800 5600 4800
Text Label 5600 4800 0    50   ~ 0
D0/RXI
Wire Wire Line
	5500 4500 5600 4500
Text Label 5600 4500 0    50   ~ 0
D4
Wire Wire Line
	8000 5700 8200 5700
Text Label 8200 5700 0    50   ~ 0
D4
Wire Wire Line
	8000 5600 8200 5600
Text Label 8200 5600 0    50   ~ 0
D3
Wire Wire Line
	5500 4600 5600 4600
Text Label 5600 4600 0    50   ~ 0
D3
Wire Wire Line
	5500 5100 5600 5100
Text Label 5600 5100 0    50   ~ 0
D2
Wire Wire Line
	8000 5500 8200 5500
Text Label 8200 5500 0    50   ~ 0
D2
Wire Wire Line
	8000 5800 8200 5800
Text Label 8150 5800 0    50   ~ 0
D5
Wire Wire Line
	5500 5200 5600 5200
Text Label 5600 5200 0    50   ~ 0
D5
Wire Wire Line
	8000 5900 8200 5900
Text Label 8200 5900 0    50   ~ 0
D6
Wire Wire Line
	5500 5700 5600 5700
Text Label 5600 5700 0    50   ~ 0
D6
Wire Wire Line
	8000 6000 8200 6000
Text Label 8200 6000 0    50   ~ 0
D7
Wire Wire Line
	5500 5800 5600 5800
Text Label 5600 5800 0    50   ~ 0
D7
Wire Wire Line
	8000 6100 8200 6100
Text Label 8200 6100 0    50   ~ 0
D8
Wire Wire Line
	2800 6000 2700 6000
Text Label 2700 6000 2    50   ~ 0
D8
Wire Wire Line
	8000 6200 8200 6200
Text Label 8200 6200 0    50   ~ 0
D9
Wire Wire Line
	2800 6100 2700 6100
Text Label 2700 6100 2    50   ~ 0
D9
Wire Wire Line
	9400 5100 9300 5100
Wire Wire Line
	9300 5100 9300 5000
Text Label 9400 5100 0    10   ~ 0
VIN
Wire Wire Line
	5200 1500 5200 1400
Wire Wire Line
	5200 1400 5400 1400
Wire Wire Line
	5400 1400 5600 1400
Wire Wire Line
	5600 1600 5400 1600
Wire Wire Line
	5400 1600 5400 1400
Wire Wire Line
	4800 1400 5200 1400
Wire Wire Line
	4800 1400 4800 1100
Wire Wire Line
	4600 1400 4700 1400
Wire Wire Line
	4700 1400 4700 1500
Wire Wire Line
	4700 1400 4800 1400
Connection ~ 5200 1400
Connection ~ 5400 1400
Connection ~ 4800 1400
Connection ~ 4700 1400
Text Label 5200 1500 0    10   ~ 0
VIN
Wire Wire Line
	4200 2600 4300 2600
Wire Wire Line
	4300 2600 4300 2500
Text Label 4200 2600 0    10   ~ 0
VIN
Wire Wire Line
	2800 2100 2800 2000
Text Label 2800 2100 0    10   ~ 0
VIN
Wire Wire Line
	9400 5500 9200 5500
Text Label 9200 5500 2    50   ~ 0
A3
Wire Wire Line
	2800 5800 2700 5800
Text Label 2700 5800 2    50   ~ 0
A3
Wire Wire Line
	9400 5600 9200 5600
Text Label 9200 5600 2    50   ~ 0
A2
Wire Wire Line
	2800 6500 2700 6500
Text Label 2700 6500 2    50   ~ 0
A2
Wire Wire Line
	9400 5700 9200 5700
Text Label 9200 5700 2    50   ~ 0
A1
Wire Wire Line
	2800 6400 2700 6400
Text Label 2700 6400 2    50   ~ 0
A1
Wire Wire Line
	9400 5800 9200 5800
Text Label 9200 5800 2    50   ~ 0
A0
Wire Wire Line
	2800 5600 2700 5600
Text Label 2700 5600 2    50   ~ 0
A0
Wire Wire Line
	9400 5900 9200 5900
Text Label 9200 5900 2    50   ~ 0
D13/SCK
Wire Wire Line
	5500 5400 5600 5400
Text Label 5600 5400 0    50   ~ 0
D13/SCK
Wire Wire Line
	7700 2500 7800 2500
Text Label 7800 2500 0    50   ~ 0
D13/SCK
Wire Wire Line
	9400 6000 9200 6000
Text Label 9200 6000 2    50   ~ 0
D12/MISO
Wire Wire Line
	5500 5600 5600 5600
Text Label 5600 5600 0    50   ~ 0
D12/MISO
Wire Wire Line
	9400 6200 9200 6200
Text Label 9200 6200 2    50   ~ 0
D10/SS
Wire Wire Line
	5500 5500 5600 5500
Text Label 5600 5500 0    50   ~ 0
D10/SS
Wire Wire Line
	9400 1800 9400 1900
Wire Wire Line
	7400 1800 7400 1900
Wire Wire Line
	5500 5300 5600 5300
Text Label 5600 5300 0    50   ~ 0
D11/MOSI
Wire Wire Line
	9400 6100 9200 6100
Text Label 9200 6100 2    50   ~ 0
D11/MOSI
Wire Wire Line
	5500 6500 5600 6500
Text Label 5600 6500 0    50   ~ 0
SWCLK
Wire Wire Line
	9600 3900 9700 3900
Text Label 9700 3900 0    50   ~ 0
SWCLK
Wire Wire Line
	5500 6600 5600 6600
Text Label 5600 6600 0    50   ~ 0
SWDIO
Wire Wire Line
	9600 3800 9700 3800
Text Label 9700 3800 0    50   ~ 0
SWDIO
Wire Wire Line
	7400 2300 7400 2200
Wire Wire Line
	5500 5900 5600 5900
Text Label 5600 5900 0    50   ~ 0
SDA
Wire Wire Line
	10400 5800 10500 5800
Text Label 10500 5800 0    50   ~ 0
SDA
Wire Wire Line
	5500 6000 5600 6000
Text Label 5600 6000 0    50   ~ 0
SCL
Wire Wire Line
	10400 5700 10500 5700
Text Label 10500 5700 0    50   ~ 0
SCL
Wire Wire Line
	2900 1400 2200 1400
Wire Wire Line
	3400 1400 4200 1400
Wire Wire Line
	3200 2600 3100 2600
Wire Wire Line
	3100 2600 3000 2600
Wire Wire Line
	3000 2600 2800 2600
Wire Wire Line
	3200 2700 3100 2700
Wire Wire Line
	3100 2700 3100 2600
Wire Wire Line
	2800 2600 2800 2500
Wire Wire Line
	2800 2600 2700 2600
Wire Wire Line
	3000 2600 3000 1700
Wire Wire Line
	3000 1700 2200 1700
Connection ~ 3100 2600
Connection ~ 2800 2600
Connection ~ 3000 2600
Wire Wire Line
	5500 6400 5600 6400
Text Label 5600 6400 0    50   ~ 0
USB_HOST_EN
Wire Wire Line
	2300 2600 2200 2600
Text Label 2200 2600 2    50   ~ 0
USB_HOST_EN
Wire Wire Line
	4200 2800 4400 2800
Wire Wire Line
	4400 2800 4400 2000
Wire Wire Line
	4400 2000 4400 1700
Wire Wire Line
	4700 1900 4700 2000
Wire Wire Line
	4700 2000 4400 2000
Connection ~ 4400 2000
Wire Wire Line
	2800 6300 2700 6300
Text Label 2700 6300 2    40   ~ 0
RX_LED
Wire Wire Line
	8700 2200 8700 2300
Wire Wire Line
	8700 2300 8800 2300
Text Label 8800 2300 0    50   ~ 0
RX_LED
Wire Wire Line
	5500 6300 5600 6300
Text Label 5600 6300 0    50   ~ 0
TX_LED
Wire Wire Line
	8000 2200 8000 2300
Wire Wire Line
	8000 2300 8100 2300
Text Label 8100 2300 0    50   ~ 0
TX_LED
Wire Wire Line
	8000 1800 8000 1900
Wire Wire Line
	8700 1800 8700 1900
Wire Wire Line
	9400 5300 9100 5300
Text Label 9100 5300 2    50   ~ 0
AREF
Wire Wire Line
	2800 5700 2700 5700
Text Label 2700 5700 2    50   ~ 0
AREF
Wire Wire Line
	9400 2300 9400 2200
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:M12NO_SILK JP1
U 1 1 0B60073D
P 7600 5600
F 0 "JP1" H 7600 6230 59  0000 L BNN
F 1 "M12NO_SILK" H 7600 4800 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:1X12_NO_SILK" H 7600 5600 50  0001 C CNN
F 3 "" H 7600 5600 50  0001 C CNN
	1    7600 5600
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:M12NO_SILK JP2
U 1 1 BAFA783C
P 9800 5700
F 0 "JP2" H 9800 6330 59  0000 L BNN
F 1 "M12NO_SILK" H 9800 4900 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:1X12_NO_SILK" H 9800 5700 50  0001 C CNN
F 3 "" H 9800 5700 50  0001 C CNN
	1    9800 5700
	-1   0    0    1   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:CRYSTAL3.2X1.5MM Y1
U 1 1 7D89E2D8
P 2500 5400
F 0 "Y1" V 2460 5320 59  0000 R TNN
F 1 "32.768kHz" V 2620 5320 59  0000 C TNN
F 2 "sparkfun-samd21-mini-breakout-v10:CRYSTAL-SMD-3.2X1.5MM" H 2500 5400 50  0001 C CNN
F 3 "" H 2500 5400 50  0001 C CNN
	1    2500 5400
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:ATSAMD21G-A U1
U 1 1 94FABF20
P 4100 5900
F 0 "U1" H 2900 7710 59  0000 L BNN
F 1 "ATSAMD21G-A" H 2900 4590 59  0000 L TNN
F 2 "sparkfun-samd21-mini-breakout-v10:TQFP-48" H 4100 5900 50  0001 C CNN
F 3 "" H 4100 5900 50  0001 C CNN
	1    4100 5900
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND01
U 1 1 8DA2BB24
P 2700 7400
F 0 "#GND01" H 2700 7400 50  0001 C CNN
F 1 "GND" H 2600 7300 59  0000 L BNN
F 2 "" H 2700 7400 50  0001 C CNN
F 3 "" H 2700 7400 50  0001 C CNN
	1    2700 7400
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:0.1UF-16V(+-10%)(0402) C1
U 1 1 AC59532D
P 2200 4700
F 0 "C1" H 2260 4815 59  0000 L BNN
F 1 "0.1uF" H 2260 4615 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0402-CAP" H 2200 4700 50  0001 C CNN
F 3 "" H 2200 4700 50  0001 C CNN
	1    2200 4700
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:0.1UF-16V(+-10%)(0402) C2
U 1 1 C8DDE19D
P 2700 4700
F 0 "C2" H 2760 4815 59  0000 L BNN
F 1 "0.1uF" H 2760 4615 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0402-CAP" H 2700 4700 50  0001 C CNN
F 3 "" H 2700 4700 50  0001 C CNN
	1    2700 4700
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:0.1UF-16V(+-10%)(0402) C3
U 1 1 88676885
P 1700 5400
F 0 "C3" H 1760 5515 59  0000 L BNN
F 1 "0.1uF" H 1760 5315 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0402-CAP" H 1700 5400 50  0001 C CNN
F 3 "" H 1700 5400 50  0001 C CNN
	1    1700 5400
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:0.1UF-16V(+-10%)(0402) C4
U 1 1 03F6E73E
P 6200 4400
F 0 "C4" H 6260 4515 59  0000 L BNN
F 1 "0.1uF" H 6260 4315 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0402-CAP" H 6200 4400 50  0001 C CNN
F 3 "" H 6200 4400 50  0001 C CNN
	1    6200 4400
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:10KOHM-1_10W-1%(0603)0603 R1
U 1 1 DE04AF3C
P 2700 3900
F 0 "R1" H 2550 3959 59  0000 L BNN
F 1 "10K" H 2550 3770 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 2700 3900 50  0001 C CNN
F 3 "" H 2700 3900 50  0001 C CNN
	1    2700 3900
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY01
U 1 1 F54A36B8
P 2700 3600
F 0 "#SUPPLY01" H 2700 3600 50  0001 C CNN
F 1 "3.3V" H 2660 3740 59  0000 L BNN
F 2 "" H 2700 3600 50  0001 C CNN
F 3 "" H 2700 3600 50  0001 C CNN
	1    2700 3600
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY02
U 1 1 19032892
P 2200 3600
F 0 "#SUPPLY02" H 2200 3600 50  0001 C CNN
F 1 "3.3V" H 2160 3740 59  0000 L BNN
F 2 "" H 2200 3600 50  0001 C CNN
F 3 "" H 2200 3600 50  0001 C CNN
	1    2200 3600
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND02
U 1 1 4877C9E2
P 2200 5000
F 0 "#GND02" H 2200 5000 50  0001 C CNN
F 1 "GND" H 2100 4900 59  0000 L BNN
F 2 "" H 2200 5000 50  0001 C CNN
F 3 "" H 2200 5000 50  0001 C CNN
	1    2200 5000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND03
U 1 1 21137572
P 2700 5000
F 0 "#GND03" H 2700 5000 50  0001 C CNN
F 1 "GND" H 2600 4900 59  0000 L BNN
F 2 "" H 2700 5000 50  0001 C CNN
F 3 "" H 2700 5000 50  0001 C CNN
	1    2700 5000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:INDUCTOR0603 L1
U 1 1 D922EA2B
P 1700 4700
F 0 "L1" H 1800 4900 59  0000 L BNN
F 1 "FB - 30Ohm" V 1660 4400 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603" H 1700 4700 50  0001 C CNN
F 3 "" H 1700 4700 50  0001 C CNN
F 4 "RES-07859" H 1700 4700 59  0001 L CNN "PROD_ID"
	1    1700 4700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND04
U 1 1 12C122FD
P 1700 5700
F 0 "#GND04" H 1700 5700 50  0001 C CNN
F 1 "GND" H 1600 5600 59  0000 L BNN
F 2 "" H 1700 5700 50  0001 C CNN
F 3 "" H 1700 5700 50  0001 C CNN
	1    1700 5700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY03
U 1 1 4F8E092B
P 1700 4300
F 0 "#SUPPLY03" H 1700 4300 50  0001 C CNN
F 1 "3.3V" H 1660 4440 59  0000 L BNN
F 2 "" H 1700 4300 50  0001 C CNN
F 3 "" H 1700 4300 50  0001 C CNN
	1    1700 4300
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:15PF-50V(+-5%)(0402) C5
U 1 1 7AD63002
P 2300 5300
F 0 "C5" V 2285 5380 59  0000 L BNN
F 1 "15pF" V 2315 5380 59  0000 L TNN
F 2 "sparkfun-samd21-mini-breakout-v10:0402-CAP" H 2300 5300 50  0001 C CNN
F 3 "" H 2300 5300 50  0001 C CNN
	1    2300 5300
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:15PF-50V(+-5%)(0402) C6
U 1 1 D57768F0
P 2200 5500
F 0 "C6" V 2185 5520 59  0000 R BNN
F 1 "15pF" V 2215 5590 59  0000 L TNN
F 2 "sparkfun-samd21-mini-breakout-v10:0402-CAP" H 2200 5500 50  0001 C CNN
F 3 "" H 2200 5500 50  0001 C CNN
	1    2200 5500
	0    1    1    0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND05
U 1 1 5B6A58CA
P 2000 5700
F 0 "#GND05" H 2000 5700 50  0001 C CNN
F 1 "GND" H 1900 5600 59  0000 L BNN
F 2 "" H 2000 5700 50  0001 C CNN
F 3 "" H 2000 5700 50  0001 C CNN
	1    2000 5700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY04
U 1 1 1C9152C2
P 6200 3800
F 0 "#SUPPLY04" H 6200 3800 50  0001 C CNN
F 1 "3.3V" H 6160 3940 59  0000 L BNN
F 2 "" H 6200 3800 50  0001 C CNN
F 3 "" H 6200 3800 50  0001 C CNN
	1    6200 3800
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND06
U 1 1 F6847280
P 6200 4700
F 0 "#GND06" H 6200 4700 50  0001 C CNN
F 1 "GND" H 6100 4600 59  0000 L BNN
F 2 "" H 6200 4700 50  0001 C CNN
F 3 "" H 6200 4700 50  0001 C CNN
	1    6200 4700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND08
U 1 1 E3161DBF
P 2300 2100
F 0 "#GND08" H 2300 2100 50  0001 C CNN
F 1 "GND" H 2200 2000 59  0000 L BNN
F 2 "" H 2300 2100 50  0001 C CNN
F 3 "" H 2300 2100 50  0001 C CNN
	1    2300 2100
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND09
U 1 1 FD0A31A1
P 2300 7100
F 0 "#GND09" H 2300 7100 50  0001 C CNN
F 1 "GND" H 2200 7000 59  0000 L BNN
F 2 "" H 2300 7100 50  0001 C CNN
F 3 "" H 2300 7100 50  0001 C CNN
	1    2300 7100
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:V_REG_AP2112K-3.3V U2
U 1 1 FB8F2D17
P 6000 1600
F 0 "U2" H 5700 1960 59  0000 L BNN
F 1 "3.3V" H 5700 1150 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:SOT23-5" H 6000 1600 50  0001 C CNN
F 3 "" H 6000 1600 50  0001 C CNN
	1    6000 1600
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND010
U 1 1 0087CD10
P 5500 2100
F 0 "#GND010" H 5500 2100 50  0001 C CNN
F 1 "GND" H 5400 2000 59  0000 L BNN
F 2 "" H 5500 2100 50  0001 C CNN
F 3 "" H 5500 2100 50  0001 C CNN
	1    5500 2100
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND011
U 1 1 5518330F
P 6500 2100
F 0 "#GND011" H 6500 2100 50  0001 C CNN
F 1 "GND" H 6400 2000 59  0000 L BNN
F 2 "" H 6500 2100 50  0001 C CNN
F 3 "" H 6500 2100 50  0001 C CNN
	1    6500 2100
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY05
U 1 1 20B1DDDE
P 6400 1300
F 0 "#SUPPLY05" H 6400 1300 50  0001 C CNN
F 1 "3.3V" H 6360 1440 59  0000 L BNN
F 2 "" H 6400 1300 50  0001 C CNN
F 3 "" H 6400 1300 50  0001 C CNN
	1    6400 1300
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND012
U 1 1 8C4C494A
P 8100 6400
F 0 "#GND012" H 8100 6400 50  0001 C CNN
F 1 "GND" H 8000 6300 59  0000 L BNN
F 2 "" H 8100 6400 50  0001 C CNN
F 3 "" H 8100 6400 50  0001 C CNN
	1    8100 6400
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND013
U 1 1 85B66688
P 9300 6400
F 0 "#GND013" H 9300 6400 50  0001 C CNN
F 1 "GND" H 9200 6300 59  0000 L BNN
F 2 "" H 9300 6400 50  0001 C CNN
F 3 "" H 9300 6400 50  0001 C CNN
	1    9300 6400
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:LED-RED0603 D2
U 1 1 975EB5E2
P 9400 2000
F 0 "D2" V 9540 1820 59  0000 L BNN
F 1 "RED" V 9625 1820 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:LED-0603" H 9400 2000 50  0001 C CNN
F 3 "" H 9400 2000 50  0001 C CNN
	1    9400 2000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:330OHM1_10W1%(0603) R2
U 1 1 3EF859C9
P 9400 1600
F 0 "R2" H 9250 1659 59  0000 L BNN
F 1 "330" H 9250 1470 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 9400 1600 50  0001 C CNN
F 3 "" H 9400 1600 50  0001 C CNN
	1    9400 1600
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:330OHM1_10W1%(0603) R3
U 1 1 1BAC543A
P 7400 1600
F 0 "R3" H 7250 1659 59  0000 L BNN
F 1 "330" H 7250 1470 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 7400 1600 50  0001 C CNN
F 3 "" H 7400 1600 50  0001 C CNN
	1    7400 1600
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY06
U 1 1 A7CD5EB6
P 9400 1300
F 0 "#SUPPLY06" H 9400 1300 50  0001 C CNN
F 1 "3.3V" H 9360 1440 59  0000 L BNN
F 2 "" H 9400 1300 50  0001 C CNN
F 3 "" H 9400 1300 50  0001 C CNN
	1    9400 1300
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND014
U 1 1 B9C9B342
P 9400 2900
F 0 "#GND014" H 9400 2900 50  0001 C CNN
F 1 "GND" H 9300 2800 59  0000 L BNN
F 2 "" H 9400 2900 50  0001 C CNN
F 3 "" H 9400 2900 50  0001 C CNN
	1    9400 2900
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND015
U 1 1 7499CEA7
P 7400 2900
F 0 "#GND015" H 7400 2900 50  0001 C CNN
F 1 "GND" H 7300 2800 59  0000 L BNN
F 2 "" H 7400 2900 50  0001 C CNN
F 3 "" H 7400 2900 50  0001 C CNN
	1    7400 2900
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:LED-BLUE0603 D3
U 1 1 B7F7A917
P 7400 2000
F 0 "D3" V 7540 1820 59  0000 L BNN
F 1 "BLUE" V 7625 1820 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:LED-0603" H 7400 2000 50  0001 C CNN
F 3 "" H 7400 2000 50  0001 C CNN
	1    7400 2000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY07
U 1 1 2907E397
P 9200 5000
F 0 "#SUPPLY07" H 9200 5000 50  0001 C CNN
F 1 "3.3V" H 9160 5140 59  0000 L BNN
F 2 "" H 9200 5000 50  0001 C CNN
F 3 "" H 9200 5000 50  0001 C CNN
	1    9200 5000
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:1.0UF-25V-+80_-20(0805)" C7
U 1 1 2B6F744E
P 5200 1700
F 0 "C7" H 5260 1815 59  0000 L BNN
F 1 "1.0uF" H 5260 1615 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0805-CAP" H 5200 1700 50  0001 C CNN
F 3 "" H 5200 1700 50  0001 C CNN
	1    5200 1700
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:2.2UF-10V-20%(0603) C8
U 1 1 6F97CAD7
P 6500 1700
F 0 "C8" H 6560 1815 59  0000 L BNN
F 1 "2.2uF" H 6560 1615 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-CAP" H 6500 1700 50  0001 C CNN
F 3 "" H 6500 1700 50  0001 C CNN
	1    6500 1700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:MOSFET-NCHANNEL2N7002PW Q1
U 1 1 08A754BB
P 7400 2500
F 0 "Q1" H 7380 2620 59  0000 R BNN
F 1 "2N7002PW" V 7550 2760 59  0000 R TNN
F 2 "sparkfun-samd21-mini-breakout-v10:SOT323" H 7400 2500 50  0001 C CNN
F 3 "" H 7400 2500 50  0001 C CNN
	1    7400 2500
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY08
U 1 1 39566F8A
P 7400 1300
F 0 "#SUPPLY08" H 7400 1300 50  0001 C CNN
F 1 "3.3V" H 7360 1440 59  0000 L BNN
F 2 "" H 7400 1300 50  0001 C CNN
F 3 "" H 7400 1300 50  0001 C CNN
	1    7400 1300
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:CORTEX_DEBUGPTH JP5
U 1 1 D97F8B07
P 8900 4000
F 0 "JP5" H 8400 4310 59  0000 L BNN
F 1 "CORTEX_DEBUGPTH" H 8400 3690 59  0000 L TNN
F 2 "sparkfun-samd21-mini-breakout-v10:2X5-PTH-1.27MM" H 8900 4000 50  0001 C CNN
F 3 "" H 8900 4000 50  0001 C CNN
	1    8900 4000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY09
U 1 1 B8640D54
P 8200 3700
F 0 "#SUPPLY09" H 8200 3700 50  0001 C CNN
F 1 "3.3V" H 8160 3840 59  0000 L BNN
F 2 "" H 8200 3700 50  0001 C CNN
F 3 "" H 8200 3700 50  0001 C CNN
	1    8200 3700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND016
U 1 1 12C6E2A1
P 8200 4400
F 0 "#GND016" H 8200 4400 50  0001 C CNN
F 1 "GND" H 8100 4300 59  0000 L BNN
F 2 "" H 8200 4400 50  0001 C CNN
F 3 "" H 8200 4400 50  0001 C CNN
	1    8200 4400
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:M021X02_NO_SILK JP4
U 1 1 193933AB
P 10100 5800
F 0 "JP4" H 10000 6030 59  0000 L BNN
F 1 "M021X02_NO_SILK" H 10000 5600 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:1X02_NO_SILK" H 10100 5800 50  0001 C CNN
F 3 "" H 10100 5800 50  0001 C CNN
	1    10100 5800
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:PTCSMD F1
U 1 1 BEAC822B
P 3100 1400
F 0 "F1" H 3000 1520 59  0000 L BNN
F 1 "500mA" H 2970 1200 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:PTC-1206" H 3100 1400 50  0001 C CNN
F 3 "" H 3100 1400 50  0001 C CNN
	1    3100 1400
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:USB-ABCONN-11794 JP3
U 1 1 8182258E
P 2000 1600
F 0 "JP3" H 2000 1600 50  0001 C CNN
F 1 "USB-ABCONN-11794" H 2000 1600 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:USB-AB-MICRO-SMD_V03" H 2000 1600 50  0001 C CNN
F 3 "" H 2000 1600 50  0001 C CNN
	1    2000 1600
	-1   0    0    1   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:74AHC1G125 U3
U 1 1 C6A83FC2
P 3700 2700
F 0 "U3" H 3410 2410 59  0000 L BNN
F 1 "74AHC1G125" H 3400 2910 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:SOT23-5" H 3700 2700 50  0001 C CNN
F 3 "" H 3700 2700 50  0001 C CNN
	1    3700 2700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:100KOHM-1_10W-1%(0603) R4
U 1 1 8EE1C540
P 2800 2300
F 0 "R4" H 2650 2359 59  0000 L BNN
F 1 "100k" H 2650 2170 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 2800 2300 50  0001 C CNN
F 3 "" H 2800 2300 50  0001 C CNN
	1    2800 2300
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:GND #GND07
U 1 1 C2693486
P 3100 3000
F 0 "#GND07" H 3100 3000 50  0001 C CNN
F 1 "GND" H 3000 2900 59  0000 L BNN
F 2 "" H 3100 3000 50  0001 C CNN
F 3 "" H 3100 3000 50  0001 C CNN
	1    3100 3000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:330OHM1_10W1%(0603) R5
U 1 1 71430D2B
P 2500 2600
F 0 "R5" H 2350 2659 59  0000 L BNN
F 1 "330" H 2350 2470 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 2500 2600 50  0001 C CNN
F 3 "" H 2500 2600 50  0001 C CNN
	1    2500 2600
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:MOSFET-PCHANNELDMG2307L Q2
U 1 1 463B9C39
P 4400 1400
F 0 "Q2" V 4370 1510 59  0000 L BNN
F 1 "2.5A/30V" V 4540 1630 59  0000 R TNN
F 2 "sparkfun-samd21-mini-breakout-v10:SOT23-3" H 4400 1400 50  0001 C CNN
F 3 "" H 4400 1400 50  0001 C CNN
	1    4400 1400
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:47KOHM1_10W1%(0603) R7
U 1 1 6661D429
P 4700 1700
F 0 "R7" H 4550 1759 59  0000 L BNN
F 1 "47K" H 4550 1570 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 4700 1700 50  0001 C CNN
F 3 "" H 4700 1700 50  0001 C CNN
	1    4700 1700
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:LED-GREEN0603 D1
U 1 1 4EA27DED
P 8000 2000
F 0 "D1" V 8140 1820 59  0000 L BNN
F 1 "GREEN" V 8225 1820 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:LED-0603" H 8000 2000 50  0001 C CNN
F 3 "" H 8000 2000 50  0001 C CNN
	1    8000 2000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:330OHM1_10W1%(0603) R6
U 1 1 A0406511
P 8000 1600
F 0 "R6" H 7850 1659 59  0000 L BNN
F 1 "330" H 7850 1470 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 8000 1600 50  0001 C CNN
F 3 "" H 8000 1600 50  0001 C CNN
	1    8000 1600
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY012
U 1 1 31C2C8A5
P 8000 1300
F 0 "#SUPPLY012" H 8000 1300 50  0001 C CNN
F 1 "3.3V" H 7960 1440 59  0000 L BNN
F 2 "" H 8000 1300 50  0001 C CNN
F 3 "" H 8000 1300 50  0001 C CNN
	1    8000 1300
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:LED-YELLOW0603 D4
U 1 1 193B20C9
P 8700 2000
F 0 "D4" V 8840 1820 59  0000 L BNN
F 1 "Yellow" V 8925 1820 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:LED-0603" H 8700 2000 50  0001 C CNN
F 3 "" H 8700 2000 50  0001 C CNN
	1    8700 2000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:330OHM1_10W1%(0603) R8
U 1 1 80905056
P 8700 1600
F 0 "R8" H 8550 1659 59  0000 L BNN
F 1 "330" H 8550 1470 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:0603-RES" H 8700 1600 50  0001 C CNN
F 3 "" H 8700 1600 50  0001 C CNN
	1    8700 1600
	0    -1   -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:3.3V #SUPPLY013
U 1 1 8D803956
P 8700 1300
F 0 "#SUPPLY013" H 8700 1300 50  0001 C CNN
F 1 "3.3V" H 8660 1440 59  0000 L BNN
F 2 "" H 8700 1300 50  0001 C CNN
F 3 "" H 8700 1300 50  0001 C CNN
	1    8700 1300
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:SFE_LOGO_FLAME.1_INCH LOGO1
U 1 1 994F8438
P 5880 7500
F 0 "LOGO1" H 5880 7500 50  0001 C CNN
F 1 "SFE_LOGO_FLAME.1_INCH" H 5880 7500 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:SFE_LOGO_FLAME_.1" H 5880 7500 50  0001 C CNN
F 3 "" H 5880 7500 50  0001 C CNN
	1    5880 7500
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:OSHW-LOGOS LOGO2
U 1 1 6CBFEB06
P 1600 7600
F 0 "LOGO2" H 1600 7600 50  0001 C CNN
F 1 "OSHW-LOGOS" H 1600 7600 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:OSHW-LOGO-S" H 1600 7600 50  0001 C CNN
F 3 "" H 1600 7600 50  0001 C CNN
	1    1600 7600
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:SFE_LOGO_NAME_FLAME.1_INCH LOGO3
U 1 1 03FD88E0
P 4600 8000
F 0 "LOGO3" H 4600 8000 50  0001 C CNN
F 1 "SFE_LOGO_NAME_FLAME.1_INCH" H 4600 8000 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:SFE_LOGO_NAME_FLAME_.1" H 4600 8000 50  0001 C CNN
F 3 "" H 4600 8000 50  0001 C CNN
	1    4600 8000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:FRAME-LETTER FRAME1
U 1 1 CC96BE83
P 1100 8000
F 0 "FRAME1" H 1100 8000 50  0001 C CNN
F 1 "FRAME-LETTER" H 1100 8000 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:CREATIVE_COMMONS" H 1100 8000 50  0001 C CNN
F 3 "" H 1100 8000 50  0001 C CNN
	1    1100 8000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:FRAME-LETTER FRAME1
U 2 1 CC96BE8F
P 6900 8000
F 0 "FRAME1" H 6900 8000 50  0001 C CNN
F 1 "FRAME-LETTER" H 6900 8000 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:CREATIVE_COMMONS" H 6900 8000 50  0001 C CNN
F 3 "" H 6900 8000 50  0001 C CNN
	2    6900 8000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:JUMPER-PAD-2-NC_BY_TRACENO_SILK SJ1
U 1 1 65C9CDFD
P 9400 2500
F 0 "SJ1" H 9300 2600 59  0000 L BNN
F 1 "PWR-LED" H 9300 2300 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:PAD-JUMPER-2-NC_BY_TRACE_NO_SILK" H 9400 2500 50  0001 C CNN
F 3 "" H 9400 2500 50  0001 C CNN
	1    9400 2500
	0    1    -1   0   
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:SWITCH-MOMENTARY-2SMD-4.6X2.8MM S1
U 1 1 001D5DFA
P 2000 6900
F 0 "S1" H 1900 7000 59  0000 L BNN
F 1 "RESET" H 1900 6800 59  0000 L BNN
F 2 "sparkfun-samd21-mini-breakout-v10:TACTILE_SWITCH_SMD_4.6X2.8MM" H 2000 6900 50  0001 C CNN
F 3 "" H 2000 6900 50  0001 C CNN
	1    2000 6900
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:FIDUCIAL1X2 FID1
U 1 1 57AF2ED6
P 10800 6800
F 0 "FID1" H 10800 6800 50  0001 C CNN
F 1 "FIDUCIAL1X2" H 10800 6800 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:FIDUCIAL-1X2" H 10800 6800 50  0001 C CNN
F 3 "" H 10800 6800 50  0001 C CNN
	1    10800 6800
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:FIDUCIAL1X2 FID2
U 1 1 AD69ACBC
P 10700 6700
F 0 "FID2" H 10700 6700 50  0001 C CNN
F 1 "FIDUCIAL1X2" H 10700 6700 50  0001 C CNN
F 2 "sparkfun-samd21-mini-breakout-v10:FIDUCIAL-1X2" H 10700 6700 50  0001 C CNN
F 3 "" H 10700 6700 50  0001 C CNN
	1    10700 6700
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:VIN #SUPPLY010
U 1 1 28999800
P 2800 2000
F 0 "#SUPPLY010" H 2800 2000 50  0001 C CNN
F 1 "VIN" H 2760 2140 59  0000 L BNN
F 2 "" H 2800 2000 50  0001 C CNN
F 3 "" H 2800 2000 50  0001 C CNN
	1    2800 2000
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:VIN #SUPPLY011
U 1 1 E0B04C0E
P 4300 2500
F 0 "#SUPPLY011" H 4300 2500 50  0001 C CNN
F 1 "VIN" H 4260 2640 59  0000 L BNN
F 2 "" H 4300 2500 50  0001 C CNN
F 3 "" H 4300 2500 50  0001 C CNN
	1    4300 2500
	-1   0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:VIN #SUPPLY014
U 1 1 C4643C99
P 4800 1100
F 0 "#SUPPLY014" H 4800 1100 50  0001 C CNN
F 1 "VIN" H 4760 1240 59  0000 L BNN
F 2 "" H 4800 1100 50  0001 C CNN
F 3 "" H 4800 1100 50  0001 C CNN
	1    4800 1100
	1    0    0    -1  
$EndComp
$Comp
L sparkfun-samd21-mini-breakout-v10-eagle-import:VIN #SUPPLY015
U 1 1 2739B52F
P 9300 5000
F 0 "#SUPPLY015" H 9300 5000 50  0001 C CNN
F 1 "VIN" H 9260 5140 59  0000 L BNN
F 2 "" H 9300 5000 50  0001 C CNN
F 3 "" H 9300 5000 50  0001 C CNN
	1    9300 5000
	1    0    0    -1  
$EndComp
Text Notes 5530 6930 0    34   ~ 0
PB22/23 are TXD/RXD to the EDBG
Text Notes 5530 6730 0    34   ~ 0
PB10/11 are MOSI/SCK to SPI header.
Text Notes 5520 4900 0    34   ~ 0
PA12 is  SPI MISO (on SPI header).
Text Notes 5520 5000 0    34   ~ 0
PA13 is DGI GPIO0 (goes to EDBG).
Text Notes 2770 5900 0    34   ~ 0
PA5 is A4
Text Notes 2770 6200 0    34   ~ 0
PB2 is A5
Wire Notes Line
	6900 4600 10900 4600
Text Notes 1200 800  0    85   ~ 0
USB / Voltage Regulator
Text Notes 7000 800  0    85   ~ 0
LEDs~~~
Text Notes 7000 3300 0    85   ~ 0
Cortex Debug/Program Connector
Text Notes 7000 4700 0    85   ~ 0
Headers
Text Notes 1200 3300 0    85   ~ 0
ATSAMD21G
Text Notes 7200 7600 0    85   ~ 0
Jim Lindblom
Text Notes 10500 7600 0    85   ~ 0
v10
Text Notes 3000 3800 0    59   ~ 0
ATSAMD21G18\nVDD Range: 1.62-3.63V
Text Notes 9700 2500 0    42   ~ 0
SJ1 enable's disconnecting\nthe Power LED indicator. For\npower-saving requirements.\nDefault: closed.
Text Notes 6500 900  0    59   ~ 0
VIN Range: 3.5-6.0V
Text Notes 10350 7460 0    59   ~ 0
Based on: \nArduino Zero by Arduino.cc\nArduino M0 by Arduino.org
Text Notes 5600 2300 0    59   ~ 0
AP2112K-3.3V\n----------------\nIout (max): 600mA \nVin (max): 6.5V\nVdrop (max): 250mV\nIq: 55uA
Wire Notes Line
	6900 700  6900 6600
Wire Notes Line
	1100 3200 10900 3200
$EndSCHEMATC
